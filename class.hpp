{% extends "cpp_header.h" %}

{% block include_guard_open %}
#ifndef {% include "header_guard_name_hpp.txt" %}
#define {% include "header_guard_name_hpp.txt" %}
{% endblock include_guard_open %}

{% block namespaces_close %}
{% include "namespace_close_cpp_named.txt" %}
{% endblock namespaces_close %}


{% block include_guard_close %}
#endif // {% include "header_guard_name_hpp.txt" %}
{% endblock include_guard_close %}